<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class QiraatTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
           DB::table('qiraats')->insert([
            'name' => 'Warsh'                  
        ]);
        DB::table('qiraats')->insert([
            'name' => 'Al Douri'
        ]);
        DB::table('qiraats')->insert([
            'name' => 'Qunbul'                 
        ]);
    }
}
