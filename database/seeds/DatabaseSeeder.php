<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
      DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Fatiha'),
            'file'=>('../qiraats/Warsh/001.mp3'),
        ]);

         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('An-Naba'),
            'file'=>('../qiraats/Warsh/078.mp3'),
        ]);

         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('An-Nazi'),
            'file'=>('../qiraats/Warsh/079.mp3'),
        ]);
        
         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Abasa'),
            'file'=>('../qiraats/Warsh/080.mp3'),
        ]);
         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('At-Takwir'),
            'file'=>('../qiraats/Warsh/081.mp3'),
        ]);

         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Infitar'),
            'file'=>('../qiraats/Warsh/082.mp3'),
        ]);

         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Mutaffifin'),
            'file'=>('../qiraats/Warsh/083.mp3'),
        ]);   
        
         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Inshiqaq'),
            'file'=>('../qiraats/Warsh/084.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Burooj'),
            'file'=>('../qiraats/Warsh/085.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Tariq'),
            'file'=>('../qiraats/Warsh/086.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-A'la"),
            'file'=>('../qiraats/Warsh/087.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Ghashiya'),
            'file'=>('../qiraats/Warsh/088.mp3'),
        ]);   
        
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Fajr'),
            'file'=>('../qiraats/Warsh/089.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Balad'),
            'file'=>('../qiraats/Warsh/090.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Ash-Shams'),
            'file'=>('../qiraats/Warsh/091.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Layl'),
            'file'=>('../qiraats/Warsh/092.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Ad-Dhuha'),
            'file'=>('../qiraats/Warsh/093.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('As-Sharh'),
            'file'=>('../qiraats/Warsh/094.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('At-Tin'),
            'file'=>('../qiraats/Warsh/095.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-'alaq"),
            'file'=>('../qiraats/Warsh/096.mp3'),
        ]);   

        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Al-Qadr"),
            'file'=>('../qiraats/Warsh/097.mp3'),
        ]);   

        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Bayyinah"),
            'file'=>('../qiraats/Warsh/098.mp3'),
        ]);   
        
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Zalzalah"),
            'file'=>('../qiraats/Warsh/099.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-adiyat"),
            'file'=>('../qiraats/Warsh/100.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-'Qari"),
            'file'=>('../qiraats/Warsh/101.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("At-Takathur"),
            'file'=>('../qiraats/Warsh/102.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Asr"),
            'file'=>('../qiraats/Warsh/103.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-HUmazah"),
            'file'=>('../qiraats/Warsh/104.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Fil"),
            'file'=>('../qiraats/Warsh/105.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Qura'ish"),
            'file'=>('../qiraats/Warsh/106.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Ma'un"),
            'file'=>('../qiraats/Warsh/107.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Kauther"),
            'file'=>('../qiraats/Warsh/108.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Kafiroon"),
            'file'=>('../qiraats/Warsh/109.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Nasr"),
            'file'=>('../qiraats/Warsh/110.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Masad"),
            'file'=>('../qiraats/Warsh/111.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-ikhlas"),
            'file'=>('../qiraats/Warsh/112.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-falaq"),
            'file'=>('qiraats/Warsh/113.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("An-Nas"),
            'file'=>('qiraats/Warsh/114.mp3'),
        ]);   
    }
}
