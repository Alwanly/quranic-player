<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class SurahsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {                        
            DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Fatiha'),
            'file'=>('../qiraat/Warsh/001.mp3'),
        ]);

         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('An-Naba'),
            'file'=>('../qiraat/Warsh/078.mp3'),
        ]);

         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('An-Nazi'),
            'file'=>('../qiraat/Warsh/079.mp3'),
        ]);
        
         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Abasa'),
            'file'=>String('../qiraat/Warsh/080.mp3'),
        ]);
         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('At-Takwir'),
            'file'=>('../qiraat/Warsh/081.mp3'),
        ]);

         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Infitar'),
            'file'=>('../qiraat/Warsh/082.mp3'),
        ]);

         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Mutaffifin'),
            'file'=>('../qiraat/Warsh/083.mp3'),
        ]);   
        
         DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Inshiqaq'),
            'file'=>('../qiraat/Warsh/084.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Burooj'),
            'file'=>('../qiraat/Warsh/085.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Tariq'),
            'file'=>('../qiraat/Warsh/086.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-A'la"),
            'file'=>('../qiraat/Warsh/087.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Ghashiya'),
            'file'=>('../qiraat/Warsh/088.mp3'),
        ]);   
        
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Fajr'),
            'file'=>('../qiraat/Warsh/089.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Balad'),
            'file'=>('../qiraat/Warsh/090.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Ash-Shams'),
            'file'=>('../qiraat/Warsh/091.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Al-Layl'),
            'file'=>('../qiraat/Warsh/092.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('Ad-Dhuha'),
            'file'=>('../qiraat/Warsh/093.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('As-Sharh'),
            'file'=>('../qiraat/Warsh/094.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>('At-Tin'),
            'file'=>('../qiraat/Warsh/095.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-'alaq"),
            'file'=>('../qiraat/Warsh/096.mp3'),
        ]);   

        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Al-Qadr"),
            'file'=>('../qiraat/Warsh/097.mp3'),
        ]);   

        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Bayyinah"),
            'file'=>('../qiraat/Warsh/098.mp3'),
        ]);   
        
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Zalzalah"),
            'file'=>('../qiraat/Warsh/099.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-adiyat"),
            'file'=>('../qiraat/Warsh/100.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-'Qari"),
            'file'=>('../qiraat/Warsh/101.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("At-Takathur"),
            'file'=>('../qiraat/Warsh/102.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Asr"),
            'file'=>('../qiraat/Warsh/103.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-HUmazah"),
            'file'=>('../qiraat/Warsh/104.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Fil"),
            'file'=>('../qiraat/Warsh/105.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Qura'ish"),
            'file'=>('../qiraat/Warsh/106.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Ma'un"),
            'file'=>('../qiraat/Warsh/107.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Kauther"),
            'file'=>('../qiraat/Warsh/108.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Kafiroon"),
            'file'=>('../qiraat/Warsh/109.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Nasr"),
            'file'=>('../qiraat/Warsh/110.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-Masad"),
            'file'=>('../qiraat/Warsh/111.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-ikhlas"),
            'file'=>('../qiraat/Warsh/112.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("Al-falaq"),
            'file'=>('../qiraat/Warsh/113.mp3'),
        ]);   
        DB::table('surahs')->insert([
            'qiraat_id' =>(1),
            'surah'=>("An-Nas"),
            'file'=>('../qiraat/Warsh/114.mp3'),
        ]);   
        
    }
}
