@extends('layouts.app')

@section('content')
<div class="container-fuild background-home">   
    <div class="container">
        <div class="row">
            <div class="col-12">     
                <div class="text">
                    <p>  Jelajahi Al-Qur’an Dengan <span class="text-qiraat"> Berbagai Macam Qira’at </span>
                    Dengan Qur’anic Player </p>        
                </div>       
            </div>
            <div class="col-12">
                <button type="button" class="btn btn-warning btn-mulai" data-toggle="modal" data-target="#exampleModal">Mulai Sekarang</button>
            </div>
            <div class="row justify-content-center">
                <div class="col-10">
                    <p class=text-quotes>
                        "Know that the truth is the Qur'an and 
                        qira’at are two different substances. 
                        The Qur'an is a revelation revealed to
                        the Prophet Muhammad as an explanation
                        and miracles, while the qira'at i
                        s lafadz-lafadz revelations
                            (Al-Qur'an), both the writing of the letters or the way they are pronounced, such as takhfif, tashdid and etc. "                         
                    </p>
                    <p class="text-quotes">
                        (Al-Zarkasyi, Al-Burhan fi Ulum Al-Qur'an, Beirut: Dar al-Ihya al-Kutub al-Arabiyah, 1957, p. 318) 
                    </p>
                </div>
            <div>
        </div>
    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <div class="modal-title judul">
        <h5>Pilih Qira'at</h5>
        </div>      
      </div>
      <div class="modal-body">
        <div class="wapper-list">
            <ul class="list-unstyled">
                @foreach($qiraats as $qiraat)
                <li class="list"><a class="list-item" href="{{ route('list.surah',['id'=>$qiraat->id]) }}">{{$qiraat->name}}</a></li>   
                @endforeach
            </ul>
        </div>        
      </div>
      <div class="modal-footer">        
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">Cancel</span>
            </button>            
      </div>  
    </div>
  </div>
</div>
@endsection
