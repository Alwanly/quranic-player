@extends('layouts.app')

@section('content')
    <div class="section-background">    
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-6">
                    <h2 class="text-judul">Surah List</h2>
                </div>
            </div>
        </div>    
    </div>
    <div class="sectiom-list-surah">
        <div class="container">            
            <div class="row justify-content surah-text">                                                                                          
                @foreach($surahs as $surah)
                <div class="col-4">
                    <p><a href="" class="link-surah"> {{$surah->surah}}</a></p>
                </div>  
                @endforeach
            </div>
            <hr>
        </div>                
    </div>    
    <div class="section-comment">
        <div class="container">
            <div class="row justify-content-md-center">
                <div class="col-6">
                    <h2 class="text-judul comment">Commention</h2>
                </div>
                <div class="col-12 form-comment">
                    <textarea class="form-control" rows="4" aria-label="With textarea"></textarea>
                    <button type="button" class="btn btn-warning btn-comment">Komentar</button>
                </div>
            </div>                        
            <div class="row justify-content-md-center">
                @for($i = 0 ; $i< 3; $i++)
                <div class="col-12 view-comment">
                    <p class="text-user">Alwan Alyafi Mulyawan</p>
                    <p class="text-comment">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                         Curabitur convallis mauris sed arcu maximus sodales. 
                         Quisque dapibus aliquam sapien, quis vulputate sapien eleifend ac. Cras sit amet urna sed enim vehicula volutpat. Curabitur non imperdiet nulla. Nunc faucibus magna ac magna porta, sit amet imperdiet ligula suscipit. Aenean in nulla ut diam consequat fermentum id sed enim. Suspendisse imperdiet metus vel consequat maximus.</p>
                    <p> <span class="text-time"> 1 Jam yang lalu </span>  <button class="btn-balas"> <img src="../img/back-vector.png" alt="">  Balas </button> </p>
                </div>
                @endfor
            </div>
        </div>
    </div>
@endsection