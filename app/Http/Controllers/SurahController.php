<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;

use Illuminate\Http\Request;

class SurahController extends Controller
{
    public function index($id){    
        $surahs['surahs']= DB::table('surahs')->where('qiraat_id',$id)->get();                                           
        return view ('list',$surahs);
    }
}
